const express = require('express');
const app = express();
const WSserver = require('express-ws')(app);
module.exports.aWss = WSserver.getWss();
const cors = require('cors');

const { handleSocket } = require('./controllers/socketController');
const { handleActiveRooms } = require('./controllers/roomsController');

const PORT = process.env.PORT || 8080;

app.use(cors());
app.use(express.json());

app.use('/api/activeRooms', handleActiveRooms);

app.ws('/', handleSocket);

app.listen(PORT, () => console.log(`Server is running on port ${PORT}`));
