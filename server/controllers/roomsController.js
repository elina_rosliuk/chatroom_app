const { getUniqueListBy } = require('../helpers/helpers');
const { aWss } = require('../index');

function getRooms(aWss) {
  const rooms = [];
  aWss.clients.forEach((client) => {
    rooms.push({ roomName: client.roomname, roomId: client.id  })
  });

  return getUniqueListBy(rooms, 'roomId');
};

module.exports.handleActiveRooms = async (req, res) => {
  let rooms;
  try {
    rooms = getRooms(aWss);
  } catch (err) {
    res.status(500).json('Server error');
  }
  res.status(200).json({rooms});
}
