const { getUniqueListBy } = require('../helpers/helpers');
const { aWss } = require('../index');

const getRoomUsers = (aWss, id) => {
  const users = [];
  aWss.clients.forEach((client) => {
    if (client.id == id) {
      users.push(client.user);
    }
  });

  return getUniqueListBy(users, 'id');
};

const broadcastConnection = (ws, aWss, message) => {
  aWss.clients.forEach((client) => {
    if (client.id == message.id) {
      if (client.readyState === ws.OPEN) {
        // TODO: check whether the user is already in the room
        const data = JSON.stringify({
          content: `The user with username ${message.username} connected to the chatroom ${message.roomname}`,
          method: 'connection',
          messageId: String(Date.now()),
        });
        client.send(data);
      }
    }
  });
};

const sendActiveUsers = (ws, aWss) => {
  const users = getRoomUsers(aWss, ws.id);

  aWss.clients.forEach((client) => {
    if (client.id == ws.id) {
      if (client.readyState === ws.OPEN) {
        const data = JSON.stringify({
          users,
          method: 'activeUsers',
        });
        client.send(data);
      }
    }
  });
}

const broadcastDisconnection = (ws, aWss) => {
  aWss.clients.forEach((client) => {
    if (client.id == ws.id) {
      if (client.readyState === ws.OPEN) {
         // TODO: check whether the user is still in the room
        const data = JSON.stringify({
          content: `${ws.user.username} left the chatroom`,
          method: 'disconnection',
          messageId: String(Date.now()),
        });
        client.send(data);
      }
    }
  });
  sendActiveUsers(ws, aWss);
};

const handleMessage = (aWss, message) => {
  aWss.clients.forEach((client) => {
    if (client.id == message.id) {
      client.send(JSON.stringify(message));
    }
  });
};

const handleConnection = (ws, aWss, message) => {
  ws.id = message.id;
  ws.roomname = message.roomname;
  ws.user = { username: message.username, id: message.userId };
  broadcastConnection(ws, aWss, message);
  sendActiveUsers(ws, aWss);
};

module.exports.handleSocket = (ws) => {
  ws.on('message', (message) => {
    message = JSON.parse(message);

    switch (message.method) {
      case 'connection':
        handleConnection(ws, aWss, message);
        break;
      case 'message':
        handleMessage(aWss, message);
        break;
    }
  });

  ws.on('close', function close() {
    broadcastDisconnection(ws, aWss);
  });
}
