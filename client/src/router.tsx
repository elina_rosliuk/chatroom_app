import React from 'react';
import { Switch, Redirect, Route } from 'react-router-dom';
import { ChatPanel } from './components/ChatPanel/ChatPanel';
import Welcome from './components/Welcome/Welcome';

export const useRoutes = () => {
  return (
    <Switch>
      <Route path="/dashboard" component={Welcome} />
      <Route path="/chatroom/:id&:room" component={ChatPanel} />
      <Redirect to="/dashboard" />
    </Switch>
  );
};
