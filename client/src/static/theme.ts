interface ITheme {
  colors: {
    [key: string]: string;
  };
}

export const theme: ITheme = {
  colors: {
    milkyWhite: '#f5f5f5',
    dirtyWhite: '#c4c4c4',
    transparentBlack: 'rgba(0, 0, 0, 0.5)',
    lightGray: '#e5e5e5',
    mint: '#9dddb8',
    brightGreen: '#15b438',
    mediumLightGray: '#afafaf',
    gray: '#525252',
    darkGray: '#333',
    orange: '#ed6755',
    peach: 'rgba(237, 103, 85, 1)',
    lightPurple: 'rgb(216, 198, 255)',
    violet: '#513372',
    lighterViolet: 'rgb(119, 91, 180)',
    hoverViolet: 'rgb(101, 71, 167)',
    shadowBlack: 'rgba(0, 0, 0, 0.3)',
    blueGray: '#8b96ac',
    transparentBlue: 'rgba(244, 239, 255, 0.3)',
  },
};
