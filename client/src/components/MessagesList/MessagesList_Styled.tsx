import styled from 'styled-components';

export const Container = styled.div`
  padding: 0.5rem;
  height: 100%;
  display: flex;
  flex-direction: column;
  overflow-y: auto;

  &::-webkit-scrollbar-track {
    box-shadow: inset 0 0 6px ${props => props.theme.colors.shadowBlack};
    -webkit-box-shadow: inset 0 0 6px ${props => props.theme.colors.shadowBlack};
    border-radius: 0.5rem;
    background-color: ${props => props.theme.colors.milkyWhite};
  }

  &::-webkit-scrollbar {
    width: 0.5rem;
    background-color: ${props => props.theme.colors.milkyWhite};
  }

  &::-webkit-scrollbar-thumb {
    border-radius: 0.5rem;
    box-shadow: inset 0 0 6px ${props => props.theme.colors.shadowBlack};
    -webkit-box-shadow: inset 0 0 6px ${props => props.theme.colors.shadowBlack};
    background-color: ${props => props.theme.colors.blueGray};
  }
`;
