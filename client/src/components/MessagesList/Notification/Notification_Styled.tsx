import styled from 'styled-components';

export const NotificationWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const NotificationText = styled.p`
  color: ${props => props.theme.colors.gray};
  font-size: 0.9rem;
`;
