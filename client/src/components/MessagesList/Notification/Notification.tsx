import React, { FC } from 'react';
import { NotificationText, NotificationWrapper } from './Notification_Styled';

interface INotification {
  lastMessage: boolean;
  setScroll: (ref: any) => void;
  children: any;
}

export const Notification: FC<INotification> = ({
  lastMessage,
  setScroll,
  children,
}) => {
  return (
    <NotificationWrapper ref={lastMessage ? setScroll : null}>
      <NotificationText>{children}</NotificationText>
    </NotificationWrapper>
  );
};
