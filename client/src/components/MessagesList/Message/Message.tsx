import { FC } from 'react';
import { IMessage } from '../MessagesList';
import { Date, Message, Sender, Text } from './Message_Styled';

interface IMessageComponent {
  lastMessage: boolean;
  setScroll: (ref: any) => void;
  children: any;
  isMessageMine: boolean;
  message: IMessage;
}

export const MessageComponent: FC<IMessageComponent> = ({
  lastMessage,
  setScroll,
  children,
  isMessageMine,
  message,
}) => {
  const dateObj: Date = new window.Date(message.date);
  const date = dateObj.toTimeString().slice(0, 5);

  return (
    // @ts-ignore
    <Message isMine={isMessageMine} ref={lastMessage ? setScroll : null}>
      {!isMessageMine && <Sender>{message.username}</Sender>}
      <Text>{children}</Text>
      <Date isMine={isMessageMine}>{date}</Date>
    </Message>
  );
};
