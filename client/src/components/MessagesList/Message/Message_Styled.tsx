import styled from 'styled-components';

export const Message = styled.div<{ isMine: boolean }>`
  margin-bottom: 10px;
  padding: 7px;
  min-width: 150px;
  width: fit-content;
  max-width: 40%;
  min-height: fit-content;
  height: fit-content;
  border: none;
  border-radius: 5px;
  color: ${({ isMine, theme }) => (isMine ? '#f3f3f3' : theme.colors.darkGray)};
  background-color: ${({ isMine, theme }) =>
    isMine ? theme.colors.violet : theme.colors.lightPurple};
  align-self: ${({ isMine }) => (isMine ? 'flex-end' : 'flex-start')};
`;

export const Text = styled.p`
  margin: 0;
  height: max-content;
  color: inherit;
  word-wrap: break-word;
`;

export const Sender = styled.p`
  margin: 0;
  margin-bottom: 0.2rem;
  font-size: 0.8rem;
  font-weight: 600;
  color: ${props => props.theme.colors.violet};
`;

export const Date = styled.p<{ isMine: boolean }>`
  margin: 0;
  margin-top: 0.2rem;
  padding: 0;
  font-size: 0.7rem;
  color: ${({ isMine, theme }) =>
    isMine ? theme.colors.mediumLightGray : theme.colors.gray};
  text-align: right;
`;
