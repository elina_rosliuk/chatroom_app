import React, { useCallback } from 'react';
import { useSelector } from 'react-redux';
import { MessageComponent as Message } from './Message/Message';
import { Container } from './MessagesList_Styled';
import { Notification } from './Notification/Notification';

type MessageMethod = 'message' | 'connection' | 'disconnection';
export interface IMessage {
  messageId: string;
  content: string;
  userId: string;
  date: Date;
  method: MessageMethod;
  username: string;
}

interface IMessagesList {
  data: IMessage[];
}

export const MessagesList: React.FC<IMessagesList> = ({ data }) => {
  const { userId } = useSelector((state: any) => state.userReducer);
  const setScrollRef = useCallback((node: any) => {
    if (node) {
      node.scrollIntoView({ smooth: true });
    }
  }, []);

  return (
    <Container>
      {data.map((message: IMessage, index: number, array: IMessage[]) => {
        const lastMessage = array.length - 1 === index;

        switch (message.method) {
          case 'message':
            const isMessageMine = userId === message.userId;
            return (
              <Message
                lastMessage={lastMessage}
                setScroll={setScrollRef}
                key={message.messageId}
                // @ts-ignore
                date={new Date(message.date)}
                message={message}
                isMessageMine={isMessageMine}
              >
                {message.content}
              </Message>
            );
          case 'connection':
          case 'disconnection':
            return (
              <Notification
                lastMessage={lastMessage}
                setScroll={setScrollRef}
                key={message.messageId}
              >
                {message.content}
              </Notification>
            );
          default:
            return null;
        }
      })}
    </Container>
  );
};
