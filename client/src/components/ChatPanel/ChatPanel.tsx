import { SocketProvider } from '../../context/SocketContext';
import { ChatField } from '../ChatField/ChatField';

export const ChatPanel = (props: any) => {
  const { id, room } = props.match.params;
  return (
    <SocketProvider>
      <ChatField key={id} paramsId={id} paramsRoom={room} />
    </SocketProvider>
  );
};
