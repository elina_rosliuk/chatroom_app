import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { RoomsList } from './AvailableRoomsList_Styled';

interface IRoom {
  roomName: string;
  roomId: string;
}

interface IAvailableRoomsList {
  rooms: IRoom[];
  showList?: boolean;
}

export const AvailableRoomsList: React.FC<IAvailableRoomsList> = ({
  rooms,
  showList = false,
}) => {
  const [showActiveRooms, setShowActiveRooms] = useState(showList);
  const history = useHistory();

  const handleRoomChoice = (roomId: string, roomName: string) => {
    const properRoomNaming = roomName.replace(/\s/g, '_');
    setShowActiveRooms(false);
    history.push(`/chatroom/${roomId}&${properRoomNaming}`);
  };

  const list =
    rooms.length &&
    rooms.map((room: IRoom) => (
      <li key={room.roomId}>
        <button onClick={() => handleRoomChoice(room.roomId, room.roomName)}>
          {room.roomName}
        </button>
      </li>
    ));

  return (
    <>
      {showActiveRooms && (
        <RoomsList>{list || <p>No rooms found</p>}</RoomsList>
      )}
    </>
  );
};
