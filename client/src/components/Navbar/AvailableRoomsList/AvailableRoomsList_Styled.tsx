import styled from 'styled-components';
import { resetDefaultButton } from '../../../helpers/resetDefaultButton';

export const RoomsList = styled.ul`
  position: absolute;
  padding: 0.7rem 1rem;
  top: 2.3rem;
  height: fit-content;
  width: 12rem;
  background-color: #f3f3f3;
  border: 1px solid ${props => props.theme.colors.dirtyWhite};
  border-radius: 0.5rem;
  box-shadow: 2px 2px 15px 0 ${props => props.theme.colors.shadowBlack};
  display: flex;
  flex-direction: column;
  z-index: 50;

  p {
    padding: 0;
    margin: 0;
  }

  li {
    margin: 0.2rem 0;
    list-style: none;
    word-wrap: break-word;
    width: 100%;

    button {
      ${resetDefaultButton};
      padding: 0.1rem 0.5rem;
      font-size: 15px;
      border-radius: 5px;
      width: 100%;
      text-align: left;
      cursor: pointer;

      :hover {
        background-color: ${props => props.theme.colors.lightGray};
      }
    }
  }
`;
