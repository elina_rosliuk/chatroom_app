import { FC, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { Link } from 'react-router-dom';
import { generateId } from '../../helpers/generateId';
import {
  setRoomId,
  setRoomName,
  setAvailableRooms,
} from '../../reducers/socketReducer/socketReducer';
import { AvailableRoomsList } from './AvailableRoomsList/AvailableRoomsList';
import Modal from '../shared/Modal/Modal';
import {
  Menu,
  MenuItem,
  NavbarWrapper,
  Logo,
  Button,
  Greeting,
  RightGroup,
} from './Navbar_Styled';

// @ts-ignore
export const Navbar: FC = () => {
  const [openModal, setOpenModal] = useState(false);
  const [showActiveRooms, setShowActiveRooms] = useState(false);
  const history = useHistory();

  const dispatch = useDispatch();
  const user = useSelector((state: any) => state.userReducer);
  const { availableRooms } = useSelector((state: any) => state.socketReducer);

  const setRoomData = (value: string): void => {
    const roomId = generateId('cr');
    const properRoomNaming = value.replace(/\s/g, '_');

    // TODO: add room name validation

    dispatch(setRoomId(roomId));
    dispatch(setRoomName(value));
    setOpenModal(false);
    history.push(`/chatroom/${roomId}&${properRoomNaming}`);
  };

  const handleCreateRoom = () => setOpenModal(true);

  const getAvailableRooms = () => {
    fetch('http://localhost:8080/api/activeRooms')
      .then(response => response.json())
      .then(data => {
        dispatch(setAvailableRooms(data.rooms));
        setShowActiveRooms(true);
      });
  };

  const handleAvailableRooms = async () =>
    showActiveRooms ? setShowActiveRooms(false) : getAvailableRooms();

  return (
    <>
      {openModal && (
        <Modal
          setData={setRoomData}
          text="Enter room name"
          placeholder="Room name"
        />
      )}
      <NavbarWrapper>
        <Logo>
          <Link className="link" to="/dashboard">
            Chatroom
          </Link>
        </Logo>
        <RightGroup>
          <Menu>
            <MenuItem>
              <Button onClick={handleAvailableRooms}>Available rooms</Button>
              {showActiveRooms && (
                <AvailableRoomsList
                  rooms={availableRooms}
                  showList={showActiveRooms}
                />
              )}
            </MenuItem>
            <MenuItem>
              <Button onClick={handleCreateRoom}>Create new room</Button>
            </MenuItem>
          </Menu>
          <Greeting>Hello, {user?.username || 'Guest'}!</Greeting>
        </RightGroup>
      </NavbarWrapper>
    </>
  );
};
