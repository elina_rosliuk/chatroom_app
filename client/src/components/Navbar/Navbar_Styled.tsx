import styled from 'styled-components';
import { resetDefaultButton } from '../../helpers/resetDefaultButton';

export const NavbarWrapper = styled.nav`
  margin: 0 auto;
  padding: 0.2rem 3rem;
  width: 90vw;
  height: 4rem;
  box-shadow: 2px 2px 15px 0 ${props => props.theme.colors.lightGray};
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const Menu = styled.ul`
  display: flex;
  align-items: center;
  list-style: none;
`;

export const MenuItem = styled.li`
  position: relative;
  margin-right: 3.5rem;
`;

export const Button = styled.button`
  ${resetDefaultButton};
  cursor: pointer;
`;

export const RightGroup = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const Logo = styled.h1``;

export const Greeting = styled.div`
  font-weight: 300;
`;
