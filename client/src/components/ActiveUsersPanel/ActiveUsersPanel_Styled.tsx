import styled from 'styled-components';

export const UserListWrapper = styled.div``;

export const UserListTitle = styled.h3`
  text-align: center;
  font-size: 1.1rem;
`;

export const UserList = styled.ul`
  list-style: none;
  font-size: 0.9rem;
`;

export const User = styled.li`
  margin: 0 1rem;
  padding: 0.3rem 0;
  border-bottom: 2px dotted ${props => props.theme.colors.mint};

  :before {
    content: '•';
    padding-right: 4px;
    vertical-align: middle;
    font-size: 1.7rem;
    color: ${props => props.theme.colors.brightGreen};
  }
`;
