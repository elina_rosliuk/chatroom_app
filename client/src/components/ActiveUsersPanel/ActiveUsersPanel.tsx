import { useSelector } from 'react-redux';
import {
  User,
  UserList,
  UserListTitle,
  UserListWrapper,
} from './ActiveUsersPanel_Styled';

interface IUser {
  id: string | number;
  username: string;
}

export const ActiveUsersPanel = () => {
  const room = useSelector((state: any) => state.socketReducer);

  const activeUsers = room.activeUsers.map((user: IUser) => (
    <User key={user.id}>{user.username}</User>
  ));

  return (
    <UserListWrapper>
      <UserListTitle>Chatroom Members</UserListTitle>
      <UserList>{activeUsers}</UserList>
    </UserListWrapper>
  );
};
