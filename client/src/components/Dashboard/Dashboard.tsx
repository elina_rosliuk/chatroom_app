import React, { FC } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { generateId } from '../../helpers/generateId';
import { setUserId, setUsername } from '../../reducers/userReducer/userReducer';
import { useRoutes } from '../../router';
import { Navbar } from '../Navbar/Navbar';
import Modal from '../shared/Modal/Modal';

// @ts-ignore
export const Dashboard: FC = () => {
  const routes = useRoutes();
  const user = useSelector((state: any) => state.userReducer);
  const dispatch = useDispatch();

  const setUser = (value: string): void => {
    const userId = generateId('user-');
    dispatch(setUsername(value));
    dispatch(setUserId(userId));
  };

  return (
    <>
      {!user?.username && (
        <Modal
          setData={setUser}
          text="Enter your name"
          placeholder="Username"
        />
      )}
      <Navbar />
      {routes}
    </>
  );
};
