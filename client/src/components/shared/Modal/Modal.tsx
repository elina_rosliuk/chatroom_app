import { createRef, SyntheticEvent, useState } from 'react';
import ReactDOM from 'react-dom';
import {
  Button,
  Input,
  ModalWrapper,
  ModalOverlay,
  ModalText,
  Form,
} from './Modal_Styled';

interface IModal {
  setData: (value: any) => void;
  placeholder: string;
  text: string;
  openModal?: boolean;
}

export default function Modal({
  setData,
  placeholder = '',
  text = '',
  openModal = true,
}: IModal) {
  const [open, setOpen] = useState(openModal);
  const inputRef = createRef<HTMLInputElement>();

  if (!open) return null;

  const handleClick = (event: SyntheticEvent) => {
    event.preventDefault();
    if (inputRef.current?.value) {
      setData(inputRef.current.value);
      setOpen(false);
    }
  };

  return ReactDOM.createPortal(
    <>
      <ModalOverlay />
      <ModalWrapper>
        <ModalText>{text}</ModalText>
        <Form>
          <Input
            ref={inputRef}
            type="text"
            placeholder={placeholder}
            maxLength={25}
          />
          <Button onClick={handleClick}>OK</Button>
        </Form>
      </ModalWrapper>
    </>,
    // @ts-ignore
    document.getElementById('portal')
  );
}
