import styled from 'styled-components';
import { resetDefaultButton } from '../../../helpers/resetDefaultButton';

export const ModalOverlay = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: ${props => props.theme.colors.transparentBlack};
  z-index: 1;
`;

export const ModalWrapper = styled.div`
  padding: 30px 30px 70px;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  background-color: #fff;
  color: #000;
  width: 35rem;
  min-height: 15rem;
  height: fit-content;
  display: flex;
  flex-direction: column;
  align-items: center;
  text-align: center;
  border-radius: 20px;
  z-index: 2;
`;

export const Button = styled.button`
  ${resetDefaultButton};
  margin-top: 2.5rem;
  padding: 0.8rem;
  background-color: ${props => props.theme.colors.orange};
  border-radius: 5px;
  width: 150px;
  font-size: 16px;
  color: white;
  box-shadow: 0px 6px 18px -5px ${props => props.theme.colors.peach};
  cursor: pointer;
`;

export const Input = styled.input`
  width: 60%;
  font-size: 1.3rem;
`;

export const ModalText = styled.p`
  font-size: 1.2rem;
`;

export const Form = styled.form`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const CloseButton = styled.button`
  ${resetDefaultButton};
  position: absolute;
  right: 1rem;
  top: 1rem;
  font-size: 20px;
  border-radius: 50%;
  width: 2rem;
  height: 2rem;
  padding-bottom: 0.2rem;
  cursor: pointer;

  :hover {
    background-color: ${props => props.theme.colors.lightGray};
  }
`;
