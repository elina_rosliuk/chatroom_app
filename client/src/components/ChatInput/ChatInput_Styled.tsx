import styled from 'styled-components';
import { resetDefaultButton } from '../../helpers/resetDefaultButton';

export const ChatInputWrapper = styled.form`
  position: absolute;
  bottom: 10px;
  left: 50%;
  transform: translate(-50%, 0);
  height: 80px;
  width: 95%;
`;

export const ChatInput = styled.textarea`
  height: 100%;
  width: 100%;
  resize: none;
`;

export const SendButton = styled.button`
  ${resetDefaultButton};
  position: absolute;
  right: 5px;
  bottom: 10px;
  cursor: pointer;
  background-color: ${props => props.theme.colors.lighterViolet};
  width: 60px;
  height: 25px;
  border-radius: 3px;
  color: #f3f3f3;

  &:hover {
    background-color: ${props => props.theme.colors.hoverViolet};
  }
`;
