import { createRef, FormEvent } from 'react';
import { useSelector } from 'react-redux';
import {
  ChatInputWrapper,
  SendButton,
  ChatInput as ChatInputField,
} from './ChatInput_Styled';

export default function ChatInput() {
  const inputRef = createRef<HTMLTextAreaElement>();
  const { username, userId } = useSelector((state: any) => state.userReducer);
  const { socket, roomId } = useSelector((state: any) => state.socketReducer);

  const handleSendMessage = (event: FormEvent) => {
    event.preventDefault();

    if (inputRef.current?.value.trim()) {
      socket.send(
        JSON.stringify({
          id: roomId,
          content: inputRef.current?.value,
          messageId: String(Date.now()),
          method: 'message',
          username: username,
          userId: userId,
          date: new Date(),
        })
      );

      inputRef.current.value = '';
    }
  };

  return (
    <ChatInputWrapper>
      <ChatInputField ref={inputRef} placeholder="Enter your message" />
      <SendButton onClick={handleSendMessage}>Send</SendButton>
    </ChatInputWrapper>
  );
}
