import { FC, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { useSocket } from '../../context/SocketContext';
import {
  setActiveUsers,
  setMessages,
  setNewRoom,
  setRoomId,
  setRoomName,
  setSocket,
} from '../../reducers/socketReducer/socketReducer';
import { ActiveUsersPanel } from '../ActiveUsersPanel/ActiveUsersPanel';
import ChatInput from '../ChatInput/ChatInput';
import { MessagesList } from '../MessagesList/MessagesList';
import { ChatArea, ChatWrapper, MainWrapper } from './ChatField_Styled';

export const ChatField: FC<any> = ({ paramsRoom, paramsId }) => {
  const { userId, username } = useSelector((state: any) => state.userReducer);
  const { socket, roomId, roomName, messages } = useSelector(
    (state: any) => state.socketReducer
  );
  const dispatch = useDispatch();
  const history = useHistory();

  // @ts-ignore
  const context: any = useSocket();
  if (context && context !== socket) dispatch(setSocket(context));

  // TODO: update component when user joins with link and set up their name

  useEffect(() => {
    if (paramsId && paramsRoom) {
      const properRoomNaming = paramsRoom.replace(/_/g, ' ');
      // TODO: add request to server to search for existing rooms
      // redirect (or show pop-up, or sth else) if no such rooms
      dispatch(setRoomId(paramsId));
      dispatch(setRoomName(properRoomNaming));
    }
    dispatch(setNewRoom());
    if (!roomId && !paramsId && !paramsRoom) {
      history.push('/');
    }
  }, [paramsId]);

  useEffect(() => {
    if (socket) {
      socket.onopen = () => {
        socket.send(
          JSON.stringify({
            id: roomId,
            userId: userId,
            username: username,
            roomname: roomName,
            method: 'connection',
          })
        );
      };

      socket.onmessage = (event: MessageEvent) => {
        const message = JSON.parse(event.data);
        switch (message.method) {
          case 'connection':
          case 'disconnection':
          case 'message':
            dispatch(setMessages(message));
            break;
          case 'activeUsers':
            dispatch(setActiveUsers(message.users));
            break;
          default:
            return;
        }
      };
    }
  }, [socket]);

  return (
    <MainWrapper className="MainWrapper">
      <ActiveUsersPanel />
      <ChatWrapper>
        <ChatArea className="ChatArea">
          <MessagesList data={messages} />
        </ChatArea>
        <ChatInput />
      </ChatWrapper>
    </MainWrapper>
  );
};
