import styled from 'styled-components';

export const MainWrapper = styled.div`
  margin-top: 3rem;
  display: grid;
  grid-template-rows: calc(100vh - 10rem);
  grid-template-columns: 280px 1fr;
`;

/* TODO: better responsive template */

export const ChatWrapper = styled.div`
  position: relative;
  background-color: ${props => props.theme.colors.transparentBlue};
  width: 100%;
`;

export const ChatArea = styled.div`
  width: 100%;
  height: calc(100% - 85px);
  padding: 20px 30px;
`;
