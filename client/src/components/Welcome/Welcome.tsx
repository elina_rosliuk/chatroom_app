import React from 'react';
import { WelcomeSection } from './Welcome_Styled';

export default function Welcome() {
  return <WelcomeSection>Welcome!</WelcomeSection>;
}
