const SET_SOCKET = 'SET_SOCKET';
const SET_ROOM_NAME = 'SET_ROOM_NAME';
const SET_ROOM_ID = 'SET_ROOM_ID';
const SET_MESSAGES = 'SET_MESSAGES';
const SET_ACTIVE_USERS = 'SET_ACTIVE_USERS';
const SET_AVAILABLE_ROOMS = 'SET_AVAILABLE_ROOMS';
const SET_NEW_ROOM = 'SET_NEW_ROOM';

interface IAction {
  type: string;
  payload?: any;
}

const defaultState = {
  socket: null,
  roomName: null,
  roomId: null,
  messages: [],
  activeUsers: [],
  availableRooms: [],
};

export default function socketReducer(state = defaultState, action: IAction) {
  switch (action.type) {
    case SET_ROOM_NAME:
      return {
        ...state,
        roomName: action.payload,
      };
    case SET_ROOM_ID:
      return {
        ...state,
        roomId: action.payload,
      };
    case SET_SOCKET:
      return {
        ...state,
        socket: action.payload,
      };
    case SET_MESSAGES:
      return {
        ...state,
        messages: [...state.messages, action.payload],
      };
    case SET_ACTIVE_USERS:
      return {
        ...state,
        activeUsers: action.payload,
      };
    case SET_AVAILABLE_ROOMS:
      return {
        ...state,
        availableRooms: action.payload,
      };
    case SET_NEW_ROOM:
      return {
        ...state,
        messages: [],
        activeUsers: [],
        availableRooms: [],
      };
    default:
      return state;
  }
}

export const setRoomName = (room: string) => ({
  type: SET_ROOM_NAME,
  payload: room,
});
export const setRoomId = (id: string) => ({ type: SET_ROOM_ID, payload: id });
export const setSocket = (socket: WebSocket) => ({
  type: SET_SOCKET,
  payload: socket,
});
export const setMessages = (messages: Array<any>) => ({
  type: SET_MESSAGES,
  payload: messages,
});
export const setActiveUsers = (activeUsers: Array<any>) => ({
  type: SET_ACTIVE_USERS,
  payload: activeUsers,
});
export const setAvailableRooms = (availableRooms: Array<any>) => ({
  type: SET_AVAILABLE_ROOMS,
  payload: availableRooms,
});
export const setNewRoom = () => ({ type: SET_NEW_ROOM });
