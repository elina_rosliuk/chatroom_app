const LOCALSTORAGE__USERNAME = 'chat-room-username';
const LOCALSTORAGE__ID = 'chat-room-id';
const SET_USERNAME = 'SET_USERNAME';
const SET_USER_ID = 'SET_USER_ID';

interface IAction {
  type: string;
  payload?: any;
}

const defaultState = {
  username:
    JSON.parse(localStorage.getItem(LOCALSTORAGE__USERNAME) as string) || null,
  userId: JSON.parse(localStorage.getItem(LOCALSTORAGE__ID) as string) || null,
};

export default function userReducer(state = defaultState, action: IAction) {
  switch (action.type) {
    case SET_USERNAME:
      localStorage.setItem(
        LOCALSTORAGE__USERNAME,
        JSON.stringify(action.payload)
      );
      return {
        ...state,
        username: action.payload,
      };
    case SET_USER_ID:
      localStorage.setItem(LOCALSTORAGE__ID, JSON.stringify(action.payload));
      return {
        ...state,
        userId: action.payload,
      };
    default:
      return state;
  }
}

export const setUsername = (username: string) => ({
  type: SET_USERNAME,
  payload: username,
});
export const setUserId = (id: string) => ({ type: SET_USER_ID, payload: id });
