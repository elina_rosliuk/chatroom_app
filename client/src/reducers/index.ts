import { combineReducers } from 'redux';
import thunk from 'redux-thunk';
import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import userReducer from './userReducer/userReducer';
import socketReducer from './socketReducer/socketReducer';

const rootReducer = combineReducers({
  userReducer,
  socketReducer,
});

export const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(thunk))
);
