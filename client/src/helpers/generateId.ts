export const generateId = (prefix: string = '') =>
  `${prefix}${(+new Date()).toString(16)}`;
