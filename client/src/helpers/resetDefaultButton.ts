import { css } from 'styled-components';

export const resetDefaultButton = css`
  border: none;
  background-color: transparent;
  font-family: inherit;
  font-size: inherit;
`;
