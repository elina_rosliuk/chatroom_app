import React, { useContext, useEffect, useState } from 'react';
import { useSelector } from 'react-redux';

// @ts-ignore
export const WebsocketContext = React.createContext();

export function useSocket() {
  return useContext(WebsocketContext);
}

interface SocketProps {
  children: any;
}

export function SocketProvider({ children }: SocketProps) {
  const [socket, setSocket] = useState<any>(null);
  const { roomId } = useSelector((state: any) => state.socketReducer);

  useEffect(() => {
    const newSocket = new WebSocket('ws://localhost:8080/'); // TODO: Remove hardcoded text
    setSocket(newSocket);

    return () => newSocket.close();
  }, [roomId]);

  return (
    <WebsocketContext.Provider value={socket}>
      {children}
    </WebsocketContext.Provider>
  );
}
